# Rcplex

**R** interface to **CPLEX** solvers for linear, quadratic, and (linear and quadratic) mixed integer programs.
Support for quadratically constrained programming is available.

## Installation

Please consult the manual of **CPLEX** on setting it up properly.
After **CPLEX** is installed the **R** package can be installed from **gitlab** via

```{r}
remotes:::install_gitlab("roigrp/solver/rcplex@add-control-argument-nthreads", INSTALL_opts = "--no-multiarch")
```

## Control arguments

- `trace` an integer controling the verbosity (default is `0L`).   
- `method` an integer (in `0` to `6`) controling the alogrithm to be used (default is `0L`),   
    + `0L` gives `CPX_ALG_AUTOMATIC`,   
    + `1L` gives `CPX_ALG_PRIMAL`,   
    + `2L` gives `CPX_ALG_DUAL`,   
    + `3L` gives `CPX_ALG_NET`,   
    + `4L` gives `CPX_ALG_BARRIER`,   
    + `5L` gives `CPX_ALG_SIFTING` and   
    + `6L` gives `CPX_ALG_CONCURRENT`.   
- `preind` an integer (in `{0, 1}`) giving weither presolve should be used (default is `1L`).   
- `aggind` an integer controling the number of analysis passes the aggregator makes (default is `-1L` in which case **CPLEX** selects the number of analysis passes).   
- `itlim` an integer controling the maximum number of iterations of the simplex algorithm.   
- `epagap` a numeric controling the absolute MIP gap tolerance (default is `0`).   
- `epgap` a numeric controling the relative MIP gap tolerance (default us `1e-4`).   
- `tilim` a numeric controling the solver time limit in seconds (default is `1e74`).   
- `disjcuts` an integer (in `{0, 1}`) controling whether disjunctive cuts should be generated (default is `0L`).   
- `mipemphasis` an integer (in `0` to `6`) controling emphasis (default is `0L`),   
    + `0L` balance optimality and feasibility
    + `1L` emphasize feasibility over optimality,
    + `2L` emphasize optimality over feasibility,
    + `3L` emphasize moving best bound and
    + `4L` emphasize finding hidden feasible solutions.   
- `cliques` an integer controling whether clique cuts should be generated (default is `0L`),
    + `-1L` generate no clique cuts,   
    + `0L` **CPLEX** chooses automatically,
    + `1L` clique cuts are generated moderately,
    + `2L` clique cuts are generated aggressively and
    + `3L` clique cuts are generated very aggressively.   
- `nodesel` an integer (default `1L`) selecting the node selection algorithm,
    + `0L` depth-first search,   
    + `1L` best-bound search,   
    + `2L` best-estimate search and   
    + `3L` alternative best-estimate search.   
- `probe` an integer (default `0L`) controling the amount of probing on variables before the MIP branching,   
    + `-1L` no probing is used,   
    + `0L` **CPLEX** chooses automatically,   
    + `1L` moderate probing is used,   
    + `2L` aggressive probing is used and   
    + `3L` very aggressive probing is used.   
- `varsel` an integer (default `0L`) controling the rule for selecting the branching variable,   
    + `-1L` branch on variable with minimum infeasibility,   
    + `0L` **CPLEX** chooses automatically,   
    + `1L` branch on variable with maximum infeasibility is used,   
    + `2L` branch based on pseudo costs is used,   
    + `3L` strong branching is used and   
    + `4L` branch based on pseudo reduced costs is used.   
- `flowcovers` an integer (default `0L`) controling whether or not to generate flow cover cuts,   
    + `-1L` do not generate flow cover cuts,   
    + `0L` **CPLEX** chooses automatically,   
    + `1L` generate flow cover cuts moderately and
    + `2L` generate flow cover cuts aggressively.
- `solnpoolagap` a numeric controling the absolute gap for the solution pool (default is `0`).       
- `solnpoolgap` a numeric controling the relative gap for the solution pool (default is `0`).             
- `solnpoolintensity` an integer (default is `0L`) controling the effort, the higher the more memory is used and time spend on the search,
    + `0L` **CPLEX** chooses the effort,   
    + `1L` no slow down no additional memory is used,
    + `2L` more memory is used,
    + `3L` more memory is used and more time is used but more solutions will be found,
    + `4L` **CPLEX** tries to find all the solutions.   
- `maxcalls` an integer controling the maximum number of calls to the **CPLEX** library before renewing the license.   
- `round` an integer (in `{0, 1}`) controling whether the integer solutions should be rounded after the optimization.   
- `nthreads` an integer giving the number of threads to be used, by default **CPLEX** selects the number of threads to be used.   
